#!/bin/bash

./gradlew bootjar -x test &&
    docker-compose build &&
    docker-compose up -d
