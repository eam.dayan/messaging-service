FROM eclipse-temurin:17.0.3_7-jre-alpine
ADD build/libs/*.jar /root.jar
CMD [ "java", "-jar", "/root.jar" ]