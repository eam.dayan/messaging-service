package com.orderup.chat.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class ClientConfig {

    @Bean
    public WebClient orderUpClient() {
        return WebClient.builder().baseUrl("https://orderup.ink").build();
    }
}
