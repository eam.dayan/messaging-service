package com.orderup.chat.common.model;

import java.time.LocalDateTime;
import java.util.UUID;

public record Group(UUID id, String name, String image, String description, LocalDateTime createdDate) {
}