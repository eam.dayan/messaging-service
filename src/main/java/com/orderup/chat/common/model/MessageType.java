package com.orderup.chat.common.model;

public enum MessageType {
    CHAT_MESSAGE, ALERT, ORDER_ARRIVED, ADDED_ITEM, CHANGED_STATUS, CANCELLED, CLOSED_ORDER
}
