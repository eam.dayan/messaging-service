package com.orderup.chat.common.model;

import java.util.UUID;

public record SimpleUser(UUID authId, String username, String name, String profileImage) {
}
