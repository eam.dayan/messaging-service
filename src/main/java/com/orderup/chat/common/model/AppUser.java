package com.orderup.chat.common.model;

import java.util.UUID;

public record AppUser(UUID authId, String username, String name, String email, String profileImage, String gender,
               String phoneNumber, String role, Boolean isActivated) {

    public SimpleUser toSimpleUser(){
        return new SimpleUser(authId, username, name, profileImage);
    }
}
