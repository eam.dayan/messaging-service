package com.orderup.chat.common.helper;

import com.orderup.chat.common.exception.GroupIdNotFoundException;
import com.orderup.chat.common.model.AppUser;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class UserServiceHelper {

    private final WebClient orderUpClient;

    public AppUser getUser(UUID userId) {
        return orderUpClient.get()
                .uri("/api/v1/users/find/" + userId)
                .retrieve()
                .bodyToMono(AppUser.class).block();
    }

    public List<AppUser> getGroupMembers(UUID groupId, String token) {
        return orderUpClient.get()
                .uri("/api/v1/groups/" + groupId + "/users")
                .headers(header -> header.add("Authorization", "Bearer " + token))
                .retrieve()
                .bodyToFlux(AppUser.class)
                .onErrorComplete(GroupIdNotFoundException.class)
                .collectList()
                .block();
    }
}

