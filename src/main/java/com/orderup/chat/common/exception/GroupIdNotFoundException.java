package com.orderup.chat.common.exception;

public class GroupIdNotFoundException extends RuntimeException {
    GroupIdNotFoundException() {
        super("cannot find group of this ID");
    }
}
