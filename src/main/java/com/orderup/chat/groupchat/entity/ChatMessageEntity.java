package com.orderup.chat.groupchat.entity;

import com.orderup.chat.common.model.SimpleUser;
import com.orderup.chat.groupchat.model.ChatRecord;
import javax.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "chat_messages")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ChatMessageEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private UUID id;

    @Column(name = "message", columnDefinition = "TEXT")
    private String message;

    @Setter(AccessLevel.NONE)
    @Column(name = "timestamp")
    private LocalDateTime timestamp;

    @Setter(AccessLevel.NONE)
    @Column(name = "sender_id", nullable = false)
    private UUID senderId;

    @Setter(AccessLevel.NONE)
    @Column(name = "group_id", nullable = false)
    private UUID groupId;

    // converts JPA entity to chat message when it displays as group chat
    public ChatRecord toDto(SimpleUser user, String jwt) {
        return new ChatRecord(id, message, timestamp, user, groupId, jwt);
    }
}