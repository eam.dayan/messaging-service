package com.orderup.chat.groupchat.repository;

import com.orderup.chat.groupchat.entity.ChatMessageEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface ChatMessageEntityRepository extends JpaRepository<ChatMessageEntity, UUID> {

    List<ChatMessageEntity> findByGroupIdOrderByTimestampDesc(UUID groupId);
}