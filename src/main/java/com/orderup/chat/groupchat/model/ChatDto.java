package com.orderup.chat.groupchat.model;

import com.orderup.chat.common.model.SimpleUser;

import java.time.LocalDateTime;
import java.util.UUID;

public record ChatDto(UUID id, String message, LocalDateTime timestamp, SimpleUser user, UUID groupId) {
}
