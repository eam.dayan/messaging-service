package com.orderup.chat.groupchat.model;

import com.orderup.chat.common.model.SimpleUser;

import java.time.LocalDateTime;
import java.util.UUID;

public record ChatRecord(UUID id, String message, LocalDateTime timestamp, SimpleUser user, UUID groupId,
                         String token) {

    public ChatDto toDto() {
        return new ChatDto(id, message, timestamp, user, groupId);
    }
}
