package com.orderup.chat.groupchat.model;


import com.orderup.chat.common.model.MessageType;
import com.orderup.chat.groupchat.entity.ChatMessageEntity;

import java.time.LocalDateTime;
import java.util.UUID;

public record ChatRequest(UUID targetGroupId, String message, MessageType type) {

    public ChatMessageEntity toEntity(UUID senderId) {
        return new ChatMessageEntity(null, message, LocalDateTime.now(), senderId, targetGroupId);
    }

}

