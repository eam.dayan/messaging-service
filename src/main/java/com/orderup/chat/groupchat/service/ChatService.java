package com.orderup.chat.groupchat.service;

import com.orderup.chat.groupchat.model.ChatDto;
import com.orderup.chat.groupchat.model.ChatRecord;
import com.orderup.chat.groupchat.model.ChatRequest;
import org.springframework.security.oauth2.jwt.Jwt;

import java.util.List;
import java.util.UUID;

public interface ChatService {

    void addMessageToGroup(ChatRequest chatRequest, Jwt jwt);

    List<ChatDto> viewGroupMessages(UUID groupId, Jwt jwt);
}
