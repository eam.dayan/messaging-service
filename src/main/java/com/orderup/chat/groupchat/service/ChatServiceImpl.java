package com.orderup.chat.groupchat.service;

import com.orderup.chat.common.helper.UserServiceHelper;
import com.orderup.chat.common.model.AppUser;
import com.orderup.chat.groupchat.entity.ChatMessageEntity;
import com.orderup.chat.groupchat.model.ChatDto;
import com.orderup.chat.groupchat.model.ChatRecord;
import com.orderup.chat.groupchat.model.ChatRequest;
import com.orderup.chat.groupchat.repository.ChatMessageEntityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ChatServiceImpl implements ChatService {

    private final ChatMessageEntityRepository chatRepository;
    private final KafkaTemplate<Integer, ChatRecord> kafkaTemplate;
    private final UserServiceHelper userService;

    @Value("${producer.kafka.topic-name}")
    private String topic;

//    @Value("${producer.kafka.group}")
//    private String group;

    /* =============================================================
     *                        SERVICE METHODS
     * =============================================================*/

    @Override
    public void addMessageToGroup(ChatRequest chatRequest, Jwt jwt) {
        // insert message
        // todo: get uuid from token
        String userId = jwt.getClaims().get("sub").toString();
        ChatMessageEntity entity = chatRequest.toEntity(UUID.fromString(userId));
        ChatMessageEntity message = chatRepository.save(entity);

        // get user (so we can display the user on the group chat)
        AppUser user = userService.getUser(message.getSenderId());

        // send it to be processed by the kafka, then websocket
        kafkaTemplate.send(topic, message.toDto(user.toSimpleUser(), jwt.getTokenValue()));
    }

    @Override
    public List<ChatDto> viewGroupMessages(UUID groupId, Jwt jwt) {
        return chatRepository.findByGroupIdOrderByTimestampDesc(groupId)
                .stream()
                .map(message -> message.toDto(userService.getUser(message.getSenderId()).toSimpleUser(), jwt.getTokenValue()))
                .map(ChatRecord::toDto)
                .collect(Collectors.toList());
    }

}
