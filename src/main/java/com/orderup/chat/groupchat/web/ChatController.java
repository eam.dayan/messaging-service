package com.orderup.chat.groupchat.web;

import com.orderup.chat.groupchat.model.ChatDto;
import com.orderup.chat.groupchat.model.ChatRequest;
import com.orderup.chat.groupchat.service.ChatService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/chat")
@RequiredArgsConstructor
@SecurityRequirement(name = "OrderUpTowerOAuth")
public class ChatController {

    private final ChatService chatService;

    @PostMapping("/group")
    public ResponseEntity<Void> sendChatMessage(@RequestBody ChatRequest chatRequest, JwtAuthenticationToken principal) {
        chatService.addMessageToGroup(chatRequest, principal.getToken());
        return ResponseEntity.ok().build();
    }

    @GetMapping("/group/{id}")
    public ResponseEntity<HashMap<String, List<ChatDto>>> fetchGroupMessages(@PathVariable("id") UUID groupId, JwtAuthenticationToken principal) {
        List<ChatDto> chatMessages = chatService.viewGroupMessages(groupId, principal.getToken());
        return ResponseEntity.ok().body(
                new HashMap<>() {{
                    put("payload", chatMessages);
                }}
        );
    }

}

