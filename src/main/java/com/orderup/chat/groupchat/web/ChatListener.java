package com.orderup.chat.groupchat.web;

import com.orderup.chat.common.helper.UserServiceHelper;
import com.orderup.chat.common.model.AppUser;
import com.orderup.chat.groupchat.model.ChatRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
public class ChatListener {

    private final SimpMessagingTemplate websocket;
    private final UserServiceHelper userService;

    public ChatListener(SimpMessagingTemplate websocket, UserServiceHelper userService) {
        this.websocket = websocket;
        this.userService = userService;
    }

    @KafkaListener(topics = "chat", groupId = "chat-group")
    public void listenChatMessages(ChatRecord data) {
        /*
         * read message send request from KAFKA
         * look for users who need to receive the data
         */
        UUID groupId = data.groupId();
        List<AppUser> appUsers = userService.getGroupMembers(groupId, data.token());

        /*
         * send the data to a topic that the user can subscribe to
         */
        assert appUsers != null;
        appUsers.forEach(user -> {
//                    SimpleUser simpleUser = user.toSimpleUser();
                    websocket.convertAndSend("/topic/chat" + user.authId(), data.toDto());
                }
        );

    }
}
